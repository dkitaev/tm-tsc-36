package ru.tsc.kitaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractProjectCommand;
import ru.tsc.kitaev.tm.endpoint.Project;
import ru.tsc.kitaev.tm.endpoint.Session;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by name...";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter Name");
        @NotNull final String name = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectEndpoint().removeProjectByName(session, name);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectTaskEndpoint().removeById(session, project.getId());
        System.out.println("[OK]");
    }

}
