package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.ISessionRepository;
import ru.tsc.kitaev.tm.api.service.ISessionService;
import ru.tsc.kitaev.tm.endpoint.Session;

public class SessionService implements ISessionService {

    private final ISessionRepository sessionRepository;

    public SessionService(@NotNull final ISessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    @Override
    @Nullable
    public Session getSession() {
        return sessionRepository.getSession();
    }

    @Override
    public void setSession(@Nullable final Session session) {
        sessionRepository.setSession(session);
    }

}
