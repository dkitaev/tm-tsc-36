package ru.tsc.kitaev.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.endpoint.Session;

public interface ISessionRepository {

    @Nullable
    Session getSession();

    void setSession(@Nullable final Session session);

}
