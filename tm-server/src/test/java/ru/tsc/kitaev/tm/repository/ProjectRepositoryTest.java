package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.repository.IProjectRepository;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.util.HashUtil;

public class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String projectName = "testProject";

    @NotNull
    private final String userId;

    public ProjectRepositoryTest() {
        @NotNull User user = new User();
        userId = user.getId();
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt("test", 5, "test"));
        project = new Project();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName(projectName);
    }

    @Before
    public void before() {
        projectRepository.add(userId, project);
    }

    @Test
    public void findProjectTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectName);
        Assert.assertEquals(project, projectRepository.findById(userId, projectId));
        Assert.assertEquals(project, projectRepository.findByIndex(userId, 0));
        Assert.assertEquals(project, projectRepository.findByName(userId, projectName));
    }

    @Test
    public void removeProjectByIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectRepository.removeById(userId, projectId);
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    public void removeProjectByIndexTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        projectRepository.removeByIndex(userId, 0);
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    public void removeProjectByNameTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectRepository.removeByName(userId, projectName);
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    public void startByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectRepository.startById(userId, projectId);
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findById(userId, projectId).getStatus());
    }

    @Test
    public void startByIndexTest() {
        Assert.assertNotNull(userId);
        projectRepository.startByIndex(userId, 0);
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void startByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectRepository.startByName(userId, projectName);
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findByName(userId, projectName).getStatus());
    }

    @Test
    public void finishByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectRepository.finishById(userId, projectId);
        Assert.assertEquals(Status.COMPLETED, projectRepository.findById(userId, projectId).getStatus());
    }

    @Test
    public void finishByIndexTest() {
        Assert.assertNotNull(userId);
        projectRepository.finishByIndex(userId, 0);
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void finishByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectRepository.finishByName(userId, projectName);
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByName(userId, projectName).getStatus());
    }

    @Test
    public void changeStatusByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectRepository.changeStatusById(userId, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findById(userId, projectId).getStatus());
        projectRepository.changeStatusById(userId, projectId, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectRepository.findById(userId, projectId).getStatus());
        projectRepository.changeStatusById(userId, projectId, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectRepository.findById(userId, projectId).getStatus());
    }

    @Test
    public void changeStatusByIndexTest() {
        Assert.assertNotNull(userId);
        projectRepository.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findByIndex(userId, 0).getStatus());
        projectRepository.changeStatusByIndex(userId, 0, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByIndex(userId, 0).getStatus());
        projectRepository.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void changeStatusByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectRepository.changeStatusByName(userId, projectName, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findByName(userId, projectName).getStatus());
        projectRepository.changeStatusByName(userId, projectName, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByName(userId, projectName).getStatus());
        projectRepository.changeStatusByName(userId, projectName, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectRepository.findByName(userId, projectName).getStatus());
    }

    @After
    public void after() {
        projectRepository.clear(userId);
    }

}
