package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.repository.IUserRepository;
import ru.tsc.kitaev.tm.model.User;

public class UserRepositoryTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final User user;

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userTest";

    @NotNull
    private final String userEmail = "test@mail.com";

    public UserRepositoryTest() {
        user = new User();
        userId = user.getId();
        user.setLogin(userLogin);
        user.setEmail(userEmail);
    }

    @Before
    public void before() {
        userRepository.add(user);
    }

    @Test
    public void findByUserTest() {
        Assert.assertEquals(user, userRepository.findById(userId));
        Assert.assertEquals(user, userRepository.findByIndex(0));
        Assert.assertEquals(user, userRepository.findByLogin(userLogin));
        Assert.assertEquals(user, userRepository.findByEmail(userEmail));
    }

    @Test
    public void removeUserTest() {
        userRepository.removeUser(user);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByLoginTest() {
        userRepository.removeByLogin(userLogin);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @After
    public void after() {
        userRepository.clear();
    }

}
