package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.repository.ISessionRepository;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.User;

public class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final Session session;

    @NotNull
    private final String sessionId;

    @NotNull
    private final String userId;

    public SessionRepositoryTest() {
        session = new Session();
        sessionId = session.getId();
        @NotNull User user = new User();
        userId = user.getId();
    }

    @Before
    public void before() {
        session.setUserId(userId);
    }

    @Test
    public void existsTest() {
        sessionRepository.add(session);
        Assert.assertTrue(sessionRepository.exists(sessionId));
    }

    @After
    public void after() {
        sessionRepository.clear();
    }

}
