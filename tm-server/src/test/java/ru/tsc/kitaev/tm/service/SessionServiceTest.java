package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.repository.ISessionRepository;
import ru.tsc.kitaev.tm.api.service.IServiceLocator;
import ru.tsc.kitaev.tm.api.service.ISessionService;
import ru.tsc.kitaev.tm.api.service.IUserService;
import ru.tsc.kitaev.tm.component.Bootstrap;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.repository.SessionRepository;

public class SessionServiceTest {

    @NotNull
    private final IServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, serviceLocator);

    @Before
    public void before() {
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @NotNull final String userId = userService.create("test", "test", "test@email.ru").getId();
        @NotNull final String adminId = userService.create("admin", "admin", Role.ADMIN).getId();
        @NotNull final Session adminSession = new Session();
        adminSession.setUserId(adminId);
        sessionService.add(adminSession);
        @NotNull final Session userSession = new Session();
        userSession.setUserId(userId);
        sessionService.add(userSession);
    }

    @Test
    public void openTest() {
        final int initialSize = sessionService.getSize();
        @NotNull final Session session = sessionService.open("test", "test");
        Assert.assertEquals(initialSize + 1, sessionService.getSize());
        Assert.assertNotNull(session.getSignature());
    }

    @Test
    public void closeTest() {
        final int initialSize = sessionService.getSize();
        @NotNull final Session session = sessionService.findAll().get(0);
        sessionService.close(session);
        Assert.assertEquals(initialSize - 1, sessionService.getSize());
    }

    @Test
    public void validateTest() {
        @NotNull final Session session = sessionService.open("admin", "admin");
        sessionService.validate(session);
    }

    @Test
    public void validateRoleTest() {
        @NotNull final Session session = sessionService.open("admin", "admin");
        sessionService.validate(session, Role.ADMIN);
    }

}
