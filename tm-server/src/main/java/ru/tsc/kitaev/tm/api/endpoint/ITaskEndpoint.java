package ru.tsc.kitaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    Task addTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "entity", partName = "entity") @Nullable Task entity
    );

    @WebMethod
    void removeTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "entity", partName = "entity") @Nullable Task entity
    );

    @WebMethod
    void clearTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    );

    @NotNull
    @WebMethod
    List<Task> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    );

    @NotNull
    @WebMethod
    List<Task> findAllTaskSorted(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "sort", partName = "sort") @Nullable String sort
    );

    @Nullable
    @WebMethod
    Task findTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    );

    @NotNull
    @WebMethod
    Task findTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    );

    @Nullable
    @WebMethod
    Task removeTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    );

    @Nullable
    @WebMethod
    Task removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    );

    @NotNull
    @WebMethod
    Task createTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description
    );

    @NotNull
    @WebMethod
    Task findTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    );

    @NotNull
    @WebMethod
    Task removeTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    );

    @Nullable
    @WebMethod
    Task updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    );

    @NotNull
    @WebMethod
    Task updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    );

    @Nullable
    @WebMethod
    Task startTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    );

    @NotNull
    @WebMethod
    Task startTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    );

    @NotNull
    @WebMethod
    Task startTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    );

    @Nullable
    @WebMethod
    Task finishTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    );

    @NotNull
    @WebMethod
    Task finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    );

    @NotNull
    @WebMethod
    Task finishTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    );

    @Nullable
    @WebMethod
    Task changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id,
            @WebParam(name = "status", partName = "status") @Nullable Status status
    );

    @NotNull
    @WebMethod
    Task changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index,
            @WebParam(name = "status", partName = "status") @Nullable Status status
    );

    @NotNull
    @WebMethod
    Task changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "status", partName = "status") @Nullable Status status
    );

    @WebMethod
    public boolean existsTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    );

    @WebMethod
    public boolean existsTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") int index
    );

}
