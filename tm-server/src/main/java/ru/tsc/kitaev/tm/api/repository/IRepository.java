package ru.tsc.kitaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.AbstractEntity;
import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    E add(@NotNull E entity);

    void addAll(@NotNull List<E> entities);

    void remove(@NotNull E entity);

    void clear();

    @NotNull
    List<E> findAll();

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator);

    @Nullable
    E findById(@NotNull String id);

    @NotNull
    E findByIndex(@NotNull Integer index);

    @Nullable
    E removeById(@NotNull String id);

    @Nullable
    E removeByIndex(@NotNull Integer index);

    boolean existsById(@NotNull String id);

    boolean existsByIndex(int index);

    int getSize();

}
