package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.ITaskRepository;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.model.Task;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    public static Predicate<Task> predicateByName(@NotNull final String name) {
        return s -> name.equals(s.getName());
    }

    @NotNull
    public static Predicate<Task> predicateByProjectId(@NotNull final String projectId) {
        return s -> projectId.equals(s.getProjectId());
    }

    @NotNull
    public static Predicate<Task> predicateByTaskId(@NotNull final String taskId) {
        return s -> taskId.equals(s.getId());
    }

    @NotNull
    @Override
    public Task findByName(@NotNull final String userId, @NotNull final String name) {
        return findAll(userId).stream()
                .filter(predicateByName(name))
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<Task> task = Optional.of(findByName(userId, name));
        task.ifPresent(this::remove);
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task startById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Optional<Task> task = Optional.ofNullable(findById(userId, id));
        task.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task startByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<Task> task = Optional.of(findByIndex(userId, index));
        task.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task startByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<Task> task = Optional.of(findByName(userId, name));
        task.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task finishById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Optional<Task> task = Optional.ofNullable(findById(userId, id));
        task.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task finishByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<Task> task = Optional.of(findByIndex(userId, index));
        task.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task finishByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<Task> task = Optional.of(findByName(userId, name));
        task.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @Nullable
    @Override
    public Task changeStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) {
        @Nullable final Optional<Task> task = Optional.ofNullable(findById(userId, id));
        task.ifPresent(p -> p.setStatus(status));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task changeStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final Status status
    ) {
        @NotNull final Optional<Task> task = Optional.of(findByIndex(userId, index));
        task.ifPresent(p -> p.setStatus(status));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task changeStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) {
        @NotNull final Optional<Task> task = Optional.of(findByName(userId, name));
        task.ifPresent(p -> p.setStatus(status));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @Nullable
    @Override
    public Task findByProjectIdAndTaskId(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        return findAll(userId).stream()
                .filter(predicateByProjectId(projectId))
                .filter(predicateByTaskId(taskId))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .filter(predicateByProjectId(projectId))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public Task bindTaskToProjectById(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        @Nullable final Task task = findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Nullable
    @Override
    public Task unbindTaskById(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        @Nullable final Task task = findByProjectIdAndTaskId(userId, projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final List<Task> listByProject = findAllTaskByProjectId(userId, projectId);
        list.removeAll(listByProject);
    }

}
