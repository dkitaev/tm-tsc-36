package ru.tsc.kitaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.service.IUserService;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminUserEndpoint {

    @Nullable
    @WebMethod
    User findByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    );

    @Nullable
    @WebMethod
    User findByEmail(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "email", partName = "email") @Nullable String email
    );

    @Nullable
    @WebMethod
    User removeUser(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "user", partName = "user") @Nullable User user
    );

    @WebMethod
    void removeByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    );

    @NotNull
    @WebMethod
    User createUser(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "email", partName = "email") @Nullable String email
    );

    @WebMethod
    void lockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    );

    @WebMethod
    void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    );

}
