package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    List<Task> findTaskByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    Task bindTaskById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @Nullable
    Task unbindTaskById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @Nullable
    Project removeById(@Nullable String userId, @Nullable String projectId);

    @Nullable
    Project removeByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Project removeByName(@Nullable String userId, @Nullable String name);

}
