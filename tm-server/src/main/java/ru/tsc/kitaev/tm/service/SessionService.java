package ru.tsc.kitaev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.ISessionRepository;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.api.service.IServiceLocator;
import ru.tsc.kitaev.tm.api.service.ISessionService;
import ru.tsc.kitaev.tm.api.service.IUserService;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.user.AccessDeniedException;
import ru.tsc.kitaev.tm.exception.user.UserIsLockedException;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.util.HashUtil;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IServiceLocator serviceLocator;

    public SessionService(
            @NotNull final ISessionRepository sessionRepository,
            @NotNull final IServiceLocator serviceLocator
    ) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public Session open(@Nullable final String login, @Nullable final String password) {
        @Nullable final User user = checkDataAccess(login, password);
        if (user == null) throw new AccessDeniedException();
        final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return sign(session);
    }

    @Nullable
    @Override
    public User checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return null;
        @NotNull final String secret = serviceLocator.getPropertyService().getPasswordSecret();
        @NotNull final Integer iteration = serviceLocator.getPropertyService().getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(secret, iteration, password);
        if (user.getLocked()) throw new UserIsLockedException();
        if (user.getPasswordHash().equals(hash)) {
            return user;
        } else {
            return null;
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session, final Role role) {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!sessionRepository.exists(session.getId())) throw new AccessDeniedException();
    }

    @NotNull
    @Override
    public Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @NotNull final String secret = serviceLocator.getPropertyService().getSessionSecret();
        @NotNull final Integer iteration = serviceLocator.getPropertyService().getSessionIteration();
        @Nullable final String signature = HashUtil.sign(secret, iteration, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    public void close(@Nullable Session session) {
        if (session == null) return;
        sessionRepository.remove(session);
    }

}
